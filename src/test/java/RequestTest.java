import java.util.Calendar;

import com.sun.jersey.api.client.ClientResponse;

import br.com.treinamento.dojo.request.GenericRequest;
import br.com.treinamento.dojo.security.MD5;
import br.com.treinamento.dojo.util.Util;

public class RequestTest extends GenericRequest {
	
	public static ClientResponse requestComics(String name) throws Exception{
		return doRequest("http://localhost:8080/hands-on/getComicsByChar?char_name="+name);
	}
}
