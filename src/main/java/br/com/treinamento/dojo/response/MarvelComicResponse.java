package br.com.treinamento.dojo.response;

import com.google.gson.JsonObject;

import br.com.treinamento.dojo.model.MarvelComic;

public class MarvelComicResponse extends MarvelResponse<MarvelComic>{

	@Override
	public MarvelComic getElements(JsonObject jsonObject) {
		MarvelComic marvelComic = new MarvelComic();
		
		marvelComic.setTitle(jsonObject.get("title").getAsString());
		marvelComic.setDigitalId(jsonObject.get("digitalId").getAsInt());
		marvelComic.setDescription(jsonObject.get("description").isJsonNull()? new String(): jsonObject.get("description").getAsString());
		
		return marvelComic;
	}

}
