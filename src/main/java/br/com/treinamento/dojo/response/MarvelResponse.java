package br.com.treinamento.dojo.response;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.ClientResponse;

import br.com.treinamento.dojo.model.MarvelModel;

public abstract class MarvelResponse<Model extends MarvelModel> {
    
    public static JsonArray parseResponse(ClientResponse response){
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject =(JsonObject) jsonParser.parse(response.getEntity(String.class));
        JsonObject data = jsonObject.getAsJsonObject("data");
        return data.getAsJsonArray("results");        
    }
    
    public List<Model> readResultJson(JsonArray results){
        List<Model> listMarvel = new ArrayList<>();
        for(JsonElement jsonElement : results){
            JsonObject jsonObject = jsonElement.getAsJsonObject();           
            listMarvel.add(getElements(jsonObject));          
        }
        
        return listMarvel;
    }

    public abstract Model getElements(JsonObject jsonObject);
    
}
