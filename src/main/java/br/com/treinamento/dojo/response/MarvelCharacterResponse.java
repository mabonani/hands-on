package br.com.treinamento.dojo.response;

import com.google.gson.JsonObject;

import br.com.treinamento.dojo.model.MarvelCharacter;

public class MarvelCharacterResponse extends MarvelResponse<MarvelCharacter> {

    @Override
    public MarvelCharacter getElements(JsonObject jsonObject) {
        MarvelCharacter marvelCharacter = new MarvelCharacter();
        marvelCharacter.setId(jsonObject.get("id").getAsLong());
        marvelCharacter.setName(jsonObject.get("name").getAsString());
        return marvelCharacter;
    }

  

}
