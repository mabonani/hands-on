package br.com.treinamento.dojo.facade.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.treinamento.dojo.facade.CharFacade;
import br.com.treinamento.dojo.model.MarvelCharacter;
import br.com.treinamento.dojo.request.MarvelRequest;
import br.com.treinamento.dojo.response.MarvelCharacterResponse;

@Service("charFacade")
@Scope("prototype")
public class CharFacadeImpl implements CharFacade<MarvelCharacter> {

    @Override
    public List<MarvelCharacter> getAllChars() {
       try {
           
           return new MarvelCharacterResponse().readResultJson(MarvelCharacterResponse.parseResponse(MarvelRequest.requestCharacters())); 
          
       } catch (Exception e) {
           return new ArrayList<>();          
       }
       
       
    }

    @Override
    public MarvelCharacter getCharByName(String name)
         {
    	List<MarvelCharacter> marvelCharacters = this.getAllChars();
        for(MarvelCharacter marvelCharacter : marvelCharacters){
            if(marvelCharacter.getName().contains(name)){
                return marvelCharacter;
            }
        }
        
        return null;
    }

   

  
}
