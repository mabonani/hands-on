package br.com.treinamento.dojo.facade.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.treinamento.dojo.facade.ComicFacade;
import br.com.treinamento.dojo.model.MarvelComic;
import br.com.treinamento.dojo.request.MarvelRequest;
import br.com.treinamento.dojo.response.MarvelCharacterResponse;
import br.com.treinamento.dojo.response.MarvelComicResponse;
@Service("comicFacade")
@Scope("prototype")
public class ComicFacadeImpl implements ComicFacade<MarvelComic>{

	@Override
	public List<MarvelComic> getComicsByCharId(long charId) {
		// TODO Auto-generated method stub
		try {
			return new MarvelComicResponse().readResultJson(MarvelCharacterResponse.parseResponse(MarvelRequest.requestComics(charId)));
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

}
