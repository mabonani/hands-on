package br.com.treinamento.dojo.facade;

import java.util.List;

import br.com.treinamento.dojo.model.MarvelComic;

public interface ComicFacade<MarvelComic> {

	List<MarvelComic> getComicsByCharId(long charId);

}
