package br.com.treinamento.dojo.facade;

import java.util.List;

public interface CharFacade<MarvelCharacter> {
	public List<MarvelCharacter> getAllChars();
	
	public br.com.treinamento.dojo.model.MarvelCharacter getCharByName(String name);

}
