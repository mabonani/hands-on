package br.com.treinamento.dojo.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.facade.CharFacade;
import br.com.treinamento.dojo.facade.ComicFacade;
import br.com.treinamento.dojo.model.MarvelCharacter;
import br.com.treinamento.dojo.model.MarvelComic;

@RestController
public class WelcomeController {

    @Autowired
    CharFacade charFacade;
    
    @Autowired
    ComicFacade comicFacade;
	
    @RequestMapping(value = "/getComicsByChar", method = RequestMethod.GET)
    public @ResponseBody List<MarvelComic> getComicsByChar(@RequestParam(name="char_name") String name) {
	    
		MarvelCharacter marvelCharacter = charFacade.getCharByName(name);
		
		if (marvelCharacter != null){
			return comicFacade.getComicsByCharId(marvelCharacter.getId());
		}
		else{
			return new ArrayList<MarvelComic>();
		}
		
	}

}
