package br.com.treinamento.dojo.request;



import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public abstract class GenericRequest {
	
	protected static ClientResponse doRequest(String url) throws Exception{
		WebResource webResource = Client.create().resource(url);
		
		ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
		
		if (response.getStatus() != 200){
		    throw new Exception("HTTP ERROR: " + response.getStatus() );
		}
		
		return response;
	}
	


}
