package br.com.treinamento.dojo.request;

import java.util.Calendar;

import com.sun.jersey.api.client.ClientResponse;

import br.com.treinamento.dojo.security.MD5;
import br.com.treinamento.dojo.util.Util;

public class MarvelRequest extends GenericRequest {
    
    
  public static ClientResponse requestCharacters() throws Exception{
	 Calendar cal = Calendar.getInstance();
	  return doRequest(Util.url_marvel+"characters?ts="+cal.getTimeInMillis()+"&apikey="+Util.PUBLIC_KEY+"&hash=" +  MD5.getMD5(cal.getTimeInMillis()+Util.PRIVATE_KEY+Util.PUBLIC_KEY)); 
   }
  
  public static ClientResponse requestComics(long charId) throws Exception{
	  Calendar cal = Calendar.getInstance();
      return doRequest(Util.url_marvel+"characters/"+charId+"/comics?ts="+cal.getTimeInMillis()+"&apikey="+Util.PUBLIC_KEY+"&hash=" +  MD5.getMD5(cal.getTimeInMillis()+Util.PRIVATE_KEY+Util.PUBLIC_KEY));
    }
}
